/** migration july 5 */

ALTER TABLE `invoices` CHANGE `due_date` `due_date` DATE NULL DEFAULT NULL;
INSERT INTO `settings` (`setting_name`, `setting_value`, `deleted`) VALUES ('company_city', '', '0'), ('company_zip', '', '0');
INSERT INTO `settings` (`setting_name`, `setting_value`, `deleted`) VALUES ('company_country', '', '0'), ('company_vat_number', '', '0');