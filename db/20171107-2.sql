ALTER TABLE `invoices` ADD `published_id` INT(11) NOT NULL AFTER `id`;
UPDATE `invoices` SET `published_id` = '1' WHERE `invoices`.`id` = 1;
UPDATE `invoices` SET `published_id` = '2' WHERE `invoices`.`id` = 2;
UPDATE `invoices` SET `published_id` = '3' WHERE `invoices`.`id` = 3;
UPDATE `invoices` SET `published_id` = '4' WHERE `invoices`.`id` = 6;
ALTER TABLE `invoices` ADD UNIQUE(`published_id`);
DELETE FROM `settings` WHERE `settings`.`setting_name` = 'company_place';
ALTER TABLE `invoices` ADD `place` TINYTEXT NOT NULL AFTER `due_date`;