<span style="font-size:20px; font-weight: bold;background-color: <?php echo $color; ?>; color: #fff;">&nbsp;<?php echo get_invoice_id($invoice_info->published_id); ?>&nbsp;</span>
<div style="line-height: 10px;"></div><?php
if (isset($invoice_info->custom_fields) && $invoice_info->custom_fields) {
    foreach ($invoice_info->custom_fields as $field) {
        if ($field->value) {
            echo "<span>" . $field->custom_field_title . ": " . $this->load->view("custom_fields/output_" . $field->custom_field_type, array("value" => $field->value), true) . "</span><br />";
        }
    }
}
?>
<?php if ($invoice_info->place !== null) : ?>
<span style="margin-right: 5px;"><?php echo lang("invoice_place") . ': ' . $invoice_info->place;?></span>
<?php endif; ?>
<?php if ($invoice_info->bill_date !== null) : ?>
<span><?php echo lang("bill_date") . ": " . format_to_date($invoice_info->bill_date, false); ?></span>
<br />
<?php endif; ?>

<?php if ($invoice_info->due_date !== null) : ?>
<span><?php echo lang("due_date") . ": " . format_to_date($invoice_info->due_date, false); ?></span>
<?php endif; ?>